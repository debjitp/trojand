#!/usr/bin/env /usr/bin/perl 
#===============================================================================
#
#         FILE: preprocess_dc_trit_ts.pl
#
#        USAGE: ./preprocess_dc_trit_ts.pl  
#
#  DESCRIPTION: This script takes a Synopsys DC netlist and converts into a Synopsys
#               DC netlist by removing the newlines and extra white spaces
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Debjit Pal (debjit.pal@cornell.edu)
# ORGANIZATION: Cornell University
#      VERSION: 1.0
#      CREATED: 12/03/19
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
use autodie;
use File::Find;
use String::Util qw(trim);
use Getopt::Long;
use Path::Tiny qw(path);
use IO::Handle;
use Array::Utils qw(:all);
use List::MoreUtils qw(first_index);
use List::MoreUtils qw(uniq);
use File::Path qw(make_path remove_tree);
use 5.010;

# Declaration of my own functions
sub mytrim($);

my $file = "dc_netlist.v";
my @module_unsynthesized;
my @logic_ele_found;

GetOptions (    "file=s"    => \$file)  # File name
or die ("Error in command line arguments\n");
print "The Synopsys Design Compiler File received: $file\n";

my $file_name = File::Spec->splitpath(+$file);

my @dir_path = split('_', substr($file_name, 0, length($file_name) - 2));
my $dir = join("/", ".", $dir_path[0], $dir_path[1]);
make_path( $dir ) or die "Could not create $dir directory, $!";

my $preprocess_filename = join("", substr($file_name, 0, length($file_name) - 2), "_preprocessed", ".v");

$preprocess_filename = join("", $dir, "/", $preprocess_filename);

print "Preprocess File Name to write: $preprocess_filename\n\n";
open(my $fh, '>', $preprocess_filename) or die "Could not open file '$preprocess_filename' $!";

my $line = '';
my $file_handle = path($file)->openr;
while ( $line = $file_handle->getline()) {
    # To remove any comment that may be in the DC netlist made by the Synopsys DC tool itself
    if ( $line =~ m/^\s*$/ ) {
        next;
    }
    if ( $line =~ m/^\/\//i ) {
        next;
    }
    my @ignore = split(m/\s+/, $line);
    if ( $ignore[0] =~ m/`timescale/ ) {
        next;
    }
    if ( $line eq '' ) {
        next;
    }
    if( $line =~ m/\bendmodule/i ) {
        print $fh $line."\n";
        last;
    }
    my $next_line = '';
    chomp($line);
    $line = mytrim($line);
    while( index($line, ";") == -1 ) {
        $next_line = mytrim($file_handle->getline());
        $line = join(" ", $line, $next_line);
        #print "$next_line\n";
        $next_line = ''
    }
    $line = mytrim($line);

    my @words = split(m/\s+/, $line);
    if ( $words[0] =~ m/nor[0-9]s[0-9]|hi[0-9]s[0-9]|nnd[0-9]s[0-9]|nb[0-9]s[0-9]|and[0-9]s[0-9]|or[0-9]s[0-9]|xor[0-9]s[0-9]|xnr[0-9]s[0-9]|i[0-9]s[0-9]|ib[0-9]s[0-9]|sdffs[0-9]|aoai[0-9]+s[0-9]|aoi[0-9]+s[0-9]|dffcs[0-9]|dffles[0-9]|dffs[0-9]|dsmxc[0-9]+s[0-9]|mx[0-9]+s[0-9]|mxi[0-9]+s[0-9]|oai[0-9]+s[0-9]/) {
        #print "Current Logic Element Found: $words[0]\n";
        print $fh $line."\n";
        push(@logic_ele_found, $words[0]);
    }
    elsif ( $words[0] =~ m/module|endmodule|input|output|wire|tri|wand|assign/ ) {
        print $fh $line."\n";
        next;
    }
    else {
        #print "Found Unknown logic element: $words[0]. Should be a synthesis problem. Please check $file_name\n";
        push(@module_unsynthesized, $words[0]);
    }
}

close $fh;

my @uniq_mod_unsyn = uniq @module_unsynthesized;
my @uniq_logic_ele_found = uniq @logic_ele_found;

print "Unique Logic elements contains in the netlist $file_name: ";
foreach my $logic_ele_found (@uniq_logic_ele_found) { 
    print "$logic_ele_found, ";
}
print "\n\n";

print "Unmapped module found in the netlist $file_name: ";
foreach my $mod_unsyn (@uniq_mod_unsyn) {
    print "$mod_unsyn, ";
}
print "\n\n";

## Trim function for removing any leading or trailing white spaces
sub mytrim($) {
  my $string = shift;
  ##print "String received: ".$string."\n";
  $string =~ s/^\s+//g;
  $string =~ s/\s+$//g;
  return $string;
}
