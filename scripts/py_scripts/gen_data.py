import numpy as np
import networkx as nx
import os
import pprint as pp
def convert(integer, length):
    bool_list = [0] * length
    bool_list[integer] = 1
    return bool_list

labels = []
train = []
node_list = []
node_type = []
G = nx.DiGraph()
save_dir = "/home/dp638/Work-x1/Trojan/gdata/"

SAMPLES = 930
TRAIN_SAMPLES = int(SAMPLES * 0.8)

for j in range(0, SAMPLES):
  if os.path.exists("/work/zhang-x1/users/dp638/Work/Trojan/graph_learning_data/num/{}.nodelist".format(j)):
    nodefile = "/work/zhang-x1/users/dp638/Work/Trojan/graph_learning_data/num/{}.nodelist".format(j)
    edgefile = "/work/zhang-x1/users/dp638/Work/Trojan/graph_learning_data/num/{}.edgelist".format(j)
    num_nodes = len(node_list)
    id_map = {}
    with open(nodefile) as ff:
        for i, line in enumerate(ff):
            info = line.split()
            id_map[info[0]] = i
            G.add_node(i+num_nodes, attribute=info[3])
            labels.append(int(info[2]))
            if j < TRAIN_SAMPLES:
                train.append(1)
            else:
                train.append(0)
            #id_map[info[0]] = int(info[0])+num_nodes
            node_list.append(i+num_nodes)
            node_type.append(info[3])
    with open(edgefile) as ff:
        for i, line in enumerate(ff):
            info = line.split()
            G.add_edge(id_map[info[0]]+num_nodes, id_map[info[1]]+num_nodes)
    print('Working on Sample ID: ', j)

nx.write_edgelist(G, save_dir+"all.edgelist")
with open(save_dir+"labels.txt", "w") as ff:
    for i in range(len(labels)):
        ff.write(str(i))
        ff.write(" ")
        ff.write(str(labels[i]))
        ff.write(" ")
        ff.write(str(train[i]))
        ff.write("\n")

all_type = {}
for x in node_type:
    if x not in all_type:
        all_type[x] = len(all_type)
num_types = len(all_type)
print('Size of gate vocubulary; ', num_types)
feats = []
for x in node_type:
    feats.append(convert(all_type[x], num_types))
features = np.array([np.array(x) for x in feats])
np.save(save_dir+"feats.npy", features)

#print(len(node_list))
#print(labels)
#print(node_type)
#print(len(G.edges()))

