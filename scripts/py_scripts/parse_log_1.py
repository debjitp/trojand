#===============================================================================
#
#         FILE: parse_log.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 20-11-2019
#     REVISION: 
#    LMODIFIED: Tue 03 Dec 2019 07:48:08 PM EST
#===============================================================================
import pprint as pp
from parse_log_5 import template

fh = open('preprocess.log', 'r')
flines = fh.readlines()

TO_LOOK_FOR_1 = 'Unique Logic elements contains in the netlist'
TO_LOOK_FOR_2 = 'Unmapped module found in the netlist'

eles_unique = []
eles_unmapped = []
counter = 0

for line in flines:
    if TO_LOOK_FOR_1 in line:
        counter = counter + 1
        idx = line.find(':')
        eline = line[idx + 1 : -1].rstrip().lstrip()
        eles_unique_ = eline.split(',')
        eles_unique_ = [e_.rstrip().lstrip() for e_ in eles_unique_ if e_]
        eles_unique = list(set(eles_unique + eles_unique_))
    elif TO_LOOK_FOR_2 in line:
        idx = line.find(':')
        eline = line[idx + 1 : -1].rstrip().lstrip()
        eles_unmapped_ = eline.split(',')
        eles_unmapped_ = [e_.rstrip().lstrip() for e_ in eles_unmapped_ if e_]
        eles_unmapped = list(set(eles_unmapped + eles_unmapped_))

pp.pprint(sorted(eles_unique))
#pp.pprint(sorted(eles_unmapped))
#print(len(eles_unique))
#print(len(eles_unmapped))
LOGICS = {}
for euniq in eles_unique:
    LOGICS[euniq] = {'IN': [], 'OUT': []}

fh = open('logic_map_trit_ts.py', 'w')
tstring = template('logic_map_trit_ts.py')
fh.write(tstring + '\n\n')
fh.write('import os, sys\n\nLOGICS = ')
pp.pprint(LOGICS, fh)
fh.close()
