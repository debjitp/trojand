#===============================================================================
#
#         FILE: parse_log_2.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 20-11-2019
#     REVISION: 
#    LMODIFIED: Wed 04 Dec 2019 03:21:23 PM EST
#===============================================================================

import os, sys

## c2670

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 100):
    file_name = 'TRIT_TC_c2670_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for c2670: Avg nodes: ' + str(nodes * 1.0 / 100) + ' Avg edges: ' + str(edges * 1.0 / 100) + \
        ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 100))

## c3540

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 100):
    file_name = 'TRIT_TC_c3540_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for c3540: Avg nodes: ' + str(nodes * 1.0 / 100) + ' Avg edges: ' + str(edges * 1.0 / 100) + \
        ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 100))

## c5315

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 100):
    file_name = 'TRIT_TC_c5315_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for c5315 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 100) + ' Avg edges: ' + \
        str(edges * 1.0 / 100) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 100))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(200, 210):
    file_name = 'TRIT_TC_c5315_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for c5315 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 10) + ' Avg edges: ' + \
        str(edges * 1.0 / 10) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 10))

## c6288

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 100):
    file_name = 'TRIT_TC_c6288_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for c6288 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 100) + ' Avg edges: ' + \
        str(edges * 1.0 / 100) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 100))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(200, 210):
    file_name = 'TRIT_TC_c6288_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for c6288 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 10) + ' Avg edges: ' + \
        str(edges * 1.0 / 10) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 10))

## s13207

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 20):
    file_name = 'TRIT_TC_s13207_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s13207 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(200, 210):
    file_name = 'TRIT_TC_s13207_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s13207 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 10) + ' Avg edges: ' + \
        str(edges * 1.0 / 10) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 10))

## s1423

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 20):
    file_name = 'TRIT_TC_s1423_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s1423 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(200, 210):
    file_name = 'TRIT_TC_s1423_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s1423 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 10) + ' Avg edges: ' + \
        str(edges * 1.0 / 10) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 10))

## s15850

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 20):
    file_name = 'TRIT_TC_s15850_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s15850 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(200, 210):
    file_name = 'TRIT_TC_s15850_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s15850 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 10) + ' Avg edges: ' + \
        str(edges * 1.0 / 10) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 10))

## s35932

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(0, 20):
    file_name = 'TRIT_TC_s35932_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s35932 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(200, 210):
    file_name = 'TRIT_TC_s35932_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s35932 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 10) + ' Avg edges: ' + \
        str(edges * 1.0 / 10) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 10))

## TRIT-TS benchmark suite

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(400, 490):
    file_name = 'TRIT_TS_s13207_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s13207 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 90) + ' Avg edges: ' + \
        str(edges * 1.0 / 90) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 90))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(600, 620):
    file_name = 'TRIT_TS_s13207_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s13207 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(400, 431):
    file_name = 'TRIT_TS_s1423_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s1423 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 31) + ' Avg edges: ' + \
        str(edges * 1.0 / 31) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 31))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(600, 620):
    file_name = 'TRIT_TS_s1423_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s1423 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))



nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(400, 490):
    file_name = 'TRIT_TS_s15850_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s15850 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 90) + ' Avg edges: ' + \
        str(edges * 1.0 / 90) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 90))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(600, 620):
    file_name = 'TRIT_TS_s15850_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s15850 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))


nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(400, 443):
    file_name = 'TRIT_TS_s35932_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s35932 (one Trojan): Avg nodes: ' + str(nodes * 1.0 / 43) + ' Avg edges: ' + \
        str(edges * 1.0 / 43) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 43))

nodes = 0
edges = 0
avg_frac_trojan = 0.0

for i in range(600, 620):
    file_name = 'TRIT_TS_s35932_T' + '0' * (3 - len(str(i))) + str(i) + '.txt'
    fh = open(file_name, 'r')
    flines = fh.readlines()
    fh.close()
    node_line = flines[-3].strip()
    edge_line = flines[-2].strip()
    trojan_line = flines[-1].strip()

    node = int(node_line[node_line.rfind(':') + 1:].strip())
    edge = int(edge_line[edge_line.rfind(':') + 1:].strip())
    avgt = float(trojan_line[trojan_line.rfind(':') + 1:].strip())

    nodes = nodes + node
    edges = edges + edge
    avg_frac_trojan = avg_frac_trojan + avgt

print('Summary for s35932 (two Trojan): Avg nodes: ' + str(nodes * 1.0 / 20) + ' Avg edges: ' + \
        str(edges * 1.0 / 20) + ' Avg Trojan fraction: ' + str(avg_frac_trojan * 1.0 / 20))

