#===============================================================================
#
#         FILE: parse_log_4.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 30-11-2019
#     REVISION: 
#    LMODIFIED: Thu 05 Dec 2019 09:08:29 PM EST
#===============================================================================

import os, sys
from collections import OrderedDict as ODict

def printTable1(myDict, colList=None):
    if not colList:
        colList = list(myDict[0].keys() if myDict else [])
    myList = [colList]

    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])

    colSize = [max(map(len, col)) for col in zip(*myList)]
    formatStr = '^  ' + '  |  '.join(["{{:^{}}}".format(i) for i in colSize])  + '  |'
    #myList.insert(1, ['-' * i for i in colSize])

    content = ''

    for item in myList:
        content = content + formatStr.format(*item) + '\n'

    return content

SOURCE_PATH = '/home/dp638/Work-x1/Trojan/glog/avg_weighted'
DEST_PATH = '/home/dp638/Work-x1/Trojan/glog/'

files = [os.path.join(SOURCE_PATH, f) for f in os.listdir(SOURCE_PATH) if f.endswith('.txt')]
files_sorted = sorted(files)
del files

stat_dict_ = {}
counter = 0

MAX_F1 = MAX_Acc = MAX_Rec = MAX_Prec = 0.0
MIN_F1 = MIN_Acc = MIN_Rec = MIN_Prec = 1.0

for file_ in files_sorted:
    fh = open(file_, 'r')
    name_ = file_[file_.rfind('/') + 1:file_.rfind('.')]
    params = name_.split('_')
    epoch = params[0]
    batch = params[1]
    chunk = params[2]
    flines = fh.readlines()
    fh.close()
    
    # Metric scores
    F1 = round(float(flines[-3][flines[-3].find(':') + 1:].lstrip().rstrip()), 5)
    Accuracy = round(float(flines[-4][flines[-4].find(':') + 1:].lstrip().rstrip()), 5)
    Recall = round(float(flines[-5][flines[-5].find(':') + 1:].lstrip().rstrip()), 5)
    Precision = round(float(flines[-6][flines[-6].find(':') + 1:].lstrip().rstrip()), 5)
    
    # Training stats
    FLOSS = round(float(flines[2][flines[2].find(':') + 1:].lstrip().rstrip()), 5)
    AVGBT = round(float(flines[4][flines[4].find(':') + 1:].lstrip().rstrip()), 5)

    stat_dict_[str(counter)] = [epoch, batch, chunk, Precision, Recall, \
            Accuracy, F1, FLOSS, AVGBT]

    counter = counter + 1
    #print(name_, epoch, batch, chunk, F1, Accuracy, Recall, Precision, FLOSS, AVGBT)

    MAX_F1 = max(MAX_F1, F1)
    MAX_Acc = max(MAX_Acc, Accuracy)
    MAX_Rec = max(MAX_Rec, Recall)
    MAX_Prec = max(MAX_Prec, Precision)

    MIN_F1 = min(MIN_F1, F1)
    MIN_Acc = min(MIN_Acc, Accuracy)
    MIN_Rec = min(MIN_Rec, Recall)
    MIN_Prec = min(MIN_Prec, Precision)

stat_dict = ODict(sorted(stat_dict_.items(), key=lambda t: t[1][0]))
del stat_dict_

th = open(DEST_PATH + '/' + 'avg_weighted_summary.table', 'w')
tcontent = printTable1(stat_dict, \
        ['No', 'epoch', 'batch', 'chunk', 'Precision', 'Recall', 'Accuracy', \
        'F1', 'FLOSS', 'AVGBT (in secs)']
        )

th.write(tcontent)
th.close()

print('MAX F1: ' + str(MAX_F1) + ', MAX Acc: ' + str(MAX_Acc) + ', MAX Rec: ' \
        + str(MAX_Rec) + ', MAX Prec: ' + str(MAX_Prec))
print('\n')
print('MIN F1: ' + str(MIN_F1) + ', MIN Acc: ' + str(MIN_Acc) + ', MIN Rec: ' \
        + str(MIN_Rec) + ', MIN Prec: ' + str(MIN_Prec))
