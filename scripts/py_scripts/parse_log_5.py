#===============================================================================
#
#         FILE: parse_log_5.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 02-12-2019
#     REVISION: 
#    LMODIFIED: Tue 03 Dec 2019 12:42:04 PM EST
#===============================================================================

import os, sys
from collections import OrderedDict as ODict
import pprint as pp
import datetime as dt
import pytz

def template(file_name):
    tstring = ''
    ffh = open('/work/zhang-x1/users/dp638/Tools/vim/templates/python_header.txt', 'r')
    flines = ffh.readlines()
    flines = flines[1:-1]
    ffh.close()

    for line in flines:
        if 'FILE' in line:
            tstring = tstring + line.strip() + ' ' + file_name + '\n'
        elif 'DESCRIPTION' in line:
            tstring = tstring + line.strip() + ' ' + 'Trojan ground truth dictionary\n'
        elif 'CREATED' in line:
            tstring = tstring + line.strip() + ' ' + dt.date.today().strftime('%d-%m-%Y') + '\n'
        elif 'LMODIFIED' in line:
            tz_NY = pytz.timezone('America/New_York')
            tstring = tstring + line.strip() + ' ' + dt.date.today().strftime('%a %d %b %Y') + ' ' + \
                    dt.datetime.now(tz_NY).strftime("%H:%M:%S") + '\n'
        else:
            tstring = tstring + line

    return tstring

SOURCE_DIR = '/home/dp638/Designs/Trojan/Abstraction/Gate/TRIT-TS/TRIT-TS'
TBODY = 'TROJAN BODY'
TRIG = 'trig'
TROJAN = 'troj'
PLOAD = 'trojan'

#files = [os.path.join(SOURCE_DIR, f, 'log.txt') for f in os.listdir(SOURCE_DIR)]
files = [f for f in os.listdir(SOURCE_DIR) if os.path.isdir(f)]
files_sorted = sorted(files)
del files

trojan_trit_ts_s13207 = {}
trojan_trit_ts_s1423 = {}
trojan_trit_ts_s15850 = {}
trojan_trit_ts_s35932 = {}

two_troj_s13207 = 0
two_troj_s1423 = 0
two_troj_s15850 = 0
two_troj_s35932 = 0

for file_ in files_sorted:
    fh = open(os.path.join(SOURCE_DIR, file_, 'log.txt'), 'r')
    flines = fh.readlines()
    fh.close()
    idx = 0
    for idx_ in range(len(flines)):
        if TBODY in flines[idx_]:
            idx = idx_
            break
    design = file_.split('_')[0]
    category = file_.split('_')[1]

    categ_dict = {}

    flines_ = flines[idx + 1:]
    TRIGGER = []
    PAYLOAD = []
    for fline in flines_:
        if not fline.strip():
            continue
        splits = fline.split()
        if len(splits) < 2:
            continue
        #print(splits)
        if PLOAD in splits[1]:
            PAYLOAD.append(splits[1])
        elif TRIG in splits[1] or TROJAN in splits[1]:
            TRIGGER.append(splits[1])

    categ_dict['Trigger'] = TRIGGER
    categ_dict['Payload'] = PAYLOAD

    if design == 's13207':
        trojan_trit_ts_s13207[category] = categ_dict
        two_troj_s13207 = two_troj_s13207 + 1 if len(PAYLOAD) > 1 else two_troj_s13207
    elif design == 's1423':
        trojan_trit_ts_s1423[category] = categ_dict
        two_troj_s1423 = two_troj_s1423 + 1 if len(PAYLOAD) > 1 else two_troj_s1423
    elif design == 's15850':
        trojan_trit_ts_s15850[category] = categ_dict
        two_troj_s15850 = two_troj_s15850 + 1 if len(PAYLOAD) > 1 else two_troj_s15850
    elif design == 's35932':
        trojan_trit_ts_s35932[category] = categ_dict
        two_troj_s35932 = two_troj_s35932 + 1 if len(PAYLOAD) > 1 else two_troj_s35932
    
    del categ_dict

fh13207 = open('trojan_ground_truth_dictionary_trit_ts_s13207.py', 'w')
tstring = template('trojan_ground_truth_dictionary_trit_ts_s13207.py')
fh13207.write(tstring + '\n\n' + 'import os, sys\n\n' + 'trojan_trit_ts_s13207 = ')
pp.pprint(trojan_trit_ts_s13207, fh13207)
fh13207.close()

fh1423 = open('trojan_ground_truth_dictionary_trit_ts_s1423.py', 'w')
tstring = template('trojan_ground_truth_dictionary_trit_ts_s1423.py')
fh1423.write(tstring + '\n\n' + 'import os, sys\n\n' + 'trojan_trit_ts_s1423 = ')
pp.pprint(trojan_trit_ts_s1423, fh1423)
fh1423.close()

fh15850 = open('trojan_ground_truth_dictionary_trit_ts_s15850.py', 'w')
tstring = template('trojan_ground_truth_dictionary_trit_ts_s15850.py')
fh15850.write(tstring + '\n\n' + 'import os, sys\n\n' + 'trojan_trit_ts_s15850 = ')
pp.pprint(trojan_trit_ts_s15850, fh15850)
fh15850.close()

fh35932 = open('trojan_ground_truth_dictionary_trit_ts_s35932.py', 'w')
tstring = template('trojan_ground_truth_dictionary_trit_ts_s35932.py')
fh35932.write(tstring + '\n\n' + 'import os, sys\n\n' + 'trojan_trit_ts_s35932 = ')
pp.pprint(trojan_trit_ts_s35932, fh35932)
fh35932.close()

print('Two Trojans in s13207 ', two_troj_s13207)
print('Two Trojans in s1423 ', two_troj_s1423)
print('Two Trojans in s15850 ', two_troj_s15850)
print('Two Trojans in s35932 ', two_troj_s35932)
