#!/bin/bash

RS_232_categ='T1000 T1100 T1200 T1300 T1400 T1500 T1600'
for categ in $RS_232_categ
do
    echo -e "\n\n"
    echo -e "Working on RS232 "$categ
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/RS232/"$categ"/uart_preprocessed_180.v -g /home/dp638/Work-x1/Trojan/img -d RS232 -c "$categ" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/RS232/$categ/uart_preprocessed_180.v -g /home/dp638/Work-x1/Trojan/img -d RS232 -c $categ  -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/RS232_$categ.txt
    echo -e "\n\n"
done

s15850_categ='T100'
for categ in $s15850_categ
do
    echo -e "\n\n"
    echo -e "Working on s15850 "$categ
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s15850/"$categ"/s15850_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s15850 -c "$categ" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s15850/$categ/s15850_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s15850 -c $categ -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/s15850_$categ.txt
    echo -e "\n\n"
done

s35932_categ='T100 T200 T300'
for categ in $s35932_categ
do
    echo -e "\n\n"
    echo -e "Working on s35932 "$categ
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s35932/"$categ"/s35932_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s35932 -c "$categ" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s35932/$categ/s35932_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s35932 -c $categ -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/s35932_$categ.txt
    echo -e "\n\n"
done

s38417_categ='T100 T200 T300'
for categ in $s38417_categ
do
    echo -e "\n\n"
    echo -e "Working on s38417 "$categ
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s38417/"$categ"/s38417_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s38417 -c "$categ" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s38417/$categ/s38417_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s38417 -c $categ -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/s38417_$categ.txt
    echo -e "\n\n"
done

s38584_categ='T100'
for categ in $s38584_categ
do
    echo -e "\n\n"
    echo -e "Working on s38584 "$categ
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s38584/"$categ"/s38584_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s38584 -c "$categ" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/s38584/$categ/s38584_scan_1_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -d s38584 -c $categ -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/s38584_$categ.txt
    echo -e "\n\n"
done

#### SUITE: TRIT-TC

##c2670
for i in $(seq -f "%03g" 0 99)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: c2670 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c2670/T"$i"/c2670_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c2670 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c2670/T$i/c2670_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c2670 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_c2670_T$i.txt
    echo -e "\n\n"
done

##c3540
for i in $(seq -f "%03g" 0 99)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: c3540 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c3540/T"$i"/c3540_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c3540 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c3540/T$i/c3540_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c3540 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_c3540_T$i.txt
    echo -e "\n\n"
done

##c5315
for i in $(seq -f "%03g" 0 99)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: c5315 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c5315/T"$i"/c5315_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c5315 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c5315/T$i/c5315_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c5315 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_c5315_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 200 209)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: c5315 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c5315/T"$i"/c5315_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c5315 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c5315/T$i/c5315_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c5315 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_c5315_T$i.txt
    echo -e "\n\n"
done

##c6288
for i in $(seq -f "%03g" 0 99)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: c6288 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c6288/T"$i"/c6288_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c6288 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c6288/T$i/c6288_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c6288 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_c6288_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 200 209)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: c6288 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c6288/T"$i"/c6288_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c6288 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/c6288/T$i/c6288_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d c6288 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_c6288_T$i.txt
    echo -e "\n\n"
done

##s13207
for i in $(seq -f "%03g" 0 19)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s13207 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s13207/T"$i"/s13207_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s13207 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s13207/T$i/s13207_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s13207 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s13207_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 200 219)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s13207 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s13207/T"$i"/s13207_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s13207 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s13207/T$i/s13207_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s13207 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s13207_T$i.txt
    echo -e "\n\n"
done

##s1423
for i in $(seq -f "%03g" 0 19)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s1423 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s1423/T"$i"/s1423_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s1423 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s1423/T$i/s1423_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s1423 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s1423_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 200 219)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s1423 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s1423/T"$i"/s1423_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s1423 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s1423/T$i/s1423_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s1423 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s1423_T$i.txt
    echo -e "\n\n"
done

##s15850
for i in $(seq -f "%03g" 0 19)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s15850 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s15850/T"$i"/s15850_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s15850 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s15850/T$i/s15850_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s15850 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s15850_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 200 219)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s15850 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s15850/T"$i"/s15850_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s15850 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s15850/T$i/s15850_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s15850 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s15850_T$i.txt
    echo -e "\n\n"
done

##s35932
for i in $(seq -f "%03g" 0 19)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s35932 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s35932/T"$i"/s35932_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s35932 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s35932/T$i/s35932_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s35932 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s35932_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 200 219)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TC DESIGN: s35932 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s35932/T"$i"/s35932_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s35932 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s35932/T$i/s35932_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TC -d s35932 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TC_s35932_T$i.txt
    echo -e "\n\n"
done

#### SUITE: TRIT-TS

##s13207
for i in $(seq -f "%03g" 400 489)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s13207 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s13207/T"$i"/s13207_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s13207 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s13207/T$i/s13207_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s13207 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s13207_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 600 619)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s13207 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s13207/T"$i"/s13207_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s13207 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s13207/T$i/s13207_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s13207 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s13207_T$i.txt
    echo -e "\n\n"
done

##s1423
for i in $(seq -f "%03g" 400 430)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s1423 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TC/s1423/T"$i"/s1423_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s1423 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s1423/T$i/s1423_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s1423 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s1423_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 600 619)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s1423 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s1423/T"$i"/s1423_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s1423 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s1423/T$i/s1423_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s1423 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s1423_T$i.txt
    echo -e "\n\n"
done

##s15850
for i in $(seq -f "%03g" 400 489)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s15850 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s15850/T"$i"/s15850_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s15850 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s15850/T$i/s15850_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s15850 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s15850_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 600 619)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s15850 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s15850/T"$i"/s15850_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s15850 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s15850/T$i/s15850_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s15850 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s15850_T$i.txt
    echo -e "\n\n"
done

##s35932
for i in $(seq -f "%03g" 400 442)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s35932 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s35932/T"$i"/s35932_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s35932 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s35932/T$i/s35932_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s35932 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s35932_T$i.txt
    echo -e "\n\n"
done

for i in $(seq -f "%03g" 600 619)
do
    echo -e "\n\n"
    echo -e "Working on SUITE: TRIT_TS DESIGN: s35932 CATEG: T"$i
    echo -e "python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s35932/T"$i"/s35932_T"$i"_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s35932 -c T"$i" -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ"
    echo -e "\n"
    python top.py -f /home/dp638/Designs/Trojan_Processed/Abstraction/Gate/TRIT-TS/s35932/T$i/s35932_T${i}_preprocessed.v -g /home/dp638/Work-x1/Trojan/img -s TRIT_TS -d s35932 -c T$i -l /home/dp638/Work-x1/Trojan/graph_learning_data/suite_des_categ &> ../log/TRIT_TS_s35932_T$i.txt
    echo -e "\n\n"
done

echo -e "All done"
