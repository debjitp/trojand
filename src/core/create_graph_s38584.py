#===============================================================================
#
#         FILE: create_graph_s38584.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 06-11-2019
#     REVISION: 
#    LMODIFIED: Fri 22 Nov 2019 09:39:32 PM EST
#===============================================================================

import os, sys
import networkx as nx
import pygraphviz as pvg
import pprint as pp

from logic.logic_map_s15850_s35932_s38417_s38584 import LOGICS as logic_s38584
from logic.logic_type_trojan import LOGIC_TYPE
from trojan.trojan_ground_truth_dictionary_s38584 import trojan_s38584

def create_graph(ngraph, floc, category):
    
    netlist = open(floc, 'r')

    trigger = trojan_s38584[category]['Trigger']
    payload = trojan_s38584[category]['Payload']

    var_def_chain, var_use_chain, inst_rnam_map = parse_netlist(ngraph, netlist, trigger, payload)
    '''   
    print('The var_def_chain is: \n')
    pp.pprint(var_def_chain)
    print('The var_use_chain is: \n')
    pp.pprint(var_use_chain)
    '''
    src_dest = {}

    complete_ngraph(ngraph, var_def_chain, var_use_chain, inst_rnam_map, src_dest, trigger, payload)
    
    return inst_rnam_map, src_dest

def complete_ngraph(ngraph, var_def_chain, var_use_chain, inst_rnam_map, src_dest, trigger, payload):

    def_vars = var_def_chain.keys()

    for def_var in def_vars:
        dinstances = var_def_chain[def_var]
        #print('dinstances for ' + def_var + ': ' + str(dinstances))
        for dinstance in dinstances:
            try:
                uinstances = var_use_chain[def_var]
                #print('uinstances for ' + def_var + ': ' + str(uinstances))
            except KeyError:
                print('>' * 3 + ' ' + def_var + ' is a primary output')
                continue
            for uinstance in uinstances:
                if not ngraph.has_edge(dinstance, uinstance):
                    if inst_rnam_map[dinstance] in trigger or inst_rnam_map[dinstance] in payload or \
                            inst_rnam_map[uinstance] in trigger or inst_rnam_map[uinstance] in payload:
                        ngraph.add_edge(dinstance, uinstance, color='green', weight=6)
                    else:
                        ngraph.add_edge(dinstance, uinstance)

                    src_dest[dinstance] = uinstance
    
    print('\n')
    print('Total number of nodes: ' + str(ngraph.number_of_nodes()))
    print('Total number of edges: ' + str(ngraph.number_of_edges()))
    print('Fraction of Trojan trigger + payload nodes: ' + str(1.0 * (len(trigger) + len(payload)) \
            /ngraph.number_of_nodes()))
    return

def parse_netlist(ngraph, netlist, trigger, payload):
 
    var_def_chain = {}
    var_use_chain = {}
    inst_rnam_map = {}

    logic_keys = logic_s38584.keys()
    counter = 0
   
    for linenumber, line in enumerate(netlist):
        try:
            key = line.split()[0]
        except IndexError:
            continue
        if key == 'module' or key == 'wire'  or key == 'assign':
            continue
        elif key == 'input':
            tline = line[6:len(line) - 1]
        elif key == 'output':
            tline = line[7:len(line) - 1]
        elif key in logic_keys:
            instance_rename = 'R' + '0' * (4 - len(str(counter))) + str(counter)
            instance_name = process(key, ngraph, line, trigger, payload, var_def_chain, var_use_chain, \
                    instance_rename)
            inst_rnam_map[instance_rename] = [
                    instance_name,
                    '1' if instance_name in trigger or instance_name in payload else '0',
                    LOGIC_TYPE[key]
                    ]
            counter = counter + 1
    netlist.close()
    return var_def_chain, var_use_chain, inst_rnam_map

def process(logic_key, ngraph, line, trigger, payload, var_def_chain, var_use_chain, instance_rename):
    
    # Name of the ports
    INPORTS = logic_s38584[logic_key]['IN']
    OUTPORTS = logic_s38584[logic_key]['OUT']

    sline = line.split()
    instance_name = sline[1]
    if instance_name in trigger:
        ngraph.add_node(instance_rename, color='red', fillcolor='red', style='filled', \
                shape='square', labels=None)
    elif instance_name in payload:
        ngraph.add_node(instance_rename, color='blue', fillcolor='blue', style='filled', \
                shape='ellipse', labels=None)
    else:
        ngraph.add_node(instance_rename, shape='circle', labels=None)

    get_var_use_chain(var_use_chain, line, INPORTS, instance_rename)
    get_var_def_chain(var_def_chain, line, OUTPORTS, instance_rename)

    return instance_name

def get_var_use_chain(var_use_chain, line, INPORTS, instance_rename):
    
    for iport in INPORTS:
        idx = line.find('.' + iport + '(')
        # Doing +2 for .(
        cidx = line[idx + len(iport) + 2:].find(')') + idx + len(iport) + 2
        input_ = line[idx + len(iport) + 2:cidx].lstrip().rstrip()
        if not input_:
            continue
        if input_ not in var_use_chain.keys():
            var_use_chain[input_] = [instance_rename]
        else:
            var_use_chain[input_].append(instance_rename)

    return

def get_var_def_chain(var_def_chain, line, OUTPORTS, instance_rename): 
        
    for oport in OUTPORTS:
        idx = line.find('.' + oport + '(')
        # Doing +2 for .(
        if idx == -1:
            continue
        cidx = line[idx + len(oport) + 2:].find(')') + idx + len(oport) + 2
        output_ = line[idx + len(oport) + 2:cidx].lstrip().rstrip()
        if not output_:
            continue
        if output_ not in var_def_chain.keys():
            var_def_chain[output_] = [instance_rename]
        else:
            var_def_chain[output_].append(instance_rename)

    return
