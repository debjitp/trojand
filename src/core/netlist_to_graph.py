#===============================================================================
#
#         FILE: netlist_to_graph.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 06-11-2019
#     REVISION: 
#    LMODIFIED: Tue 03 Dec 2019 02:41:01 AM EST
#===============================================================================

import sys, os
import networkx as nx
import pygraphviz as pgv
from datetime import datetime as dt
from argparse import ArgumentParser

from core.create_graph_RS232 import create_graph as cg_RS232
from core.create_graph_s15850 import create_graph as cg_s15850
from core.create_graph_s35932 import create_graph as cg_s35932
from core.create_graph_s38417 import create_graph as cg_s38417
from core.create_graph_s38584 import create_graph as cg_s38584
from core.create_graph_trit_tc import create_graph as cg_trit_tc
from core.create_graph_trit_ts import create_graph as cg_trit_ts

from core.plot_graph import plot_graph, printTable1, printTable2

def main(floc, gloc, lloc, suite, design, category, plot):
    
    ngraph = nx.DiGraph()

    start_time = dt.now()
    
    if not suite:
        if design == 'RS232':
            inst_rnam_map, src_dest = cg_RS232(ngraph, floc, category)
        elif design == 's15850':
            inst_rnam_map, src_dest = cg_s15850(ngraph, floc, category)
        elif design == 's35932':
            inst_rnam_map, src_dest = cg_s35932(ngraph, floc, category)
        elif design == 's38417':
            inst_rnam_map, src_dest = cg_s38417(ngraph, floc, category)
        elif design == 's38584':
            inst_rnam_map, src_dest = cg_s38584(ngraph, floc, category)
    else:
        if suite == 'TRIT_TC':
            inst_rnam_map, src_dest = cg_trit_tc(ngraph, floc, design, category)
        elif suite == 'TRIT_TS':
            inst_rnam_map, src_dest = cg_trit_ts(ngraph, floc, design, category)
    
    end_time = dt.now()
    
    icontent = printTable1(inst_rnam_map, ['Renamed instance', 'Instance name', 'Ground Truth', 'Node Type'])
    if not suite:
        ifile = open(gloc + '/' + design + '_' + category + '.map', 'w')
    else:
        ifile = open(gloc + '/' + suite + '_' + design + '_' + category + '.map', 'w')

    ifile.write(icontent)
    ifile.close()

    sdcontent = printTable2(src_dest, ['Source Node', 'Dest Node'])
    if not suite:
        sdfile = open(lloc + '/' + design + '_' + category + '.edgelist', 'w')
    else:
        sdfile = open(lloc + '/' + suite + '_' + design + '_' + category + '.edgelist', 'w')

    sdfile.write(sdcontent)
    sdfile.close()

    ncontent = printTable2(inst_rnam_map, ['Renamed instance', 'Instance name', 'Ground Truth', 'Node Type'])
    if not suite:
        nfile = open(lloc + '/' + design + '_' + category + '.nodelist', 'w')
    else:
        nfile = open(lloc + '/' + suite + '_' + design + '_' + category + '.nodelist', 'w')

    nfile.write(ncontent)
    nfile.close()

    if plot:
        plot_graph(ngraph, gloc, suite, design, category)

    return
