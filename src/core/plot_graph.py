#===============================================================================
#
#         FILE: plot_graph.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 06-11-2019
#     REVISION: 
#    LMODIFIED: Fri 22 Nov 2019 09:23:13 PM EST
#===============================================================================

from networkx.drawing.nx_agraph import *
import networkx as nx
import pygraphviz as pvg
import subprocess as sbp

def plot_graph(ngraph, gloc, suite, design, category):

    dot_file_name = gloc + '/' + design + '_' + category + '.dot' if not suite else \
            gloc + '/' + suite + '_' + design + '_' + category + '.dot' 
    A = to_agraph(ngraph)
    A.layout()
    A.draw(dot_file_name)
    
    #file_type = ['png', 'pdf']
    file_type = ['pdf']
    
    for typ in file_type:
        typ_file_name = dot_file_name[:dot_file_name.rfind('.')] + '.' + typ
        dot_cmd = 'dot -T' + typ + ' ' + dot_file_name + ' -o ' + typ_file_name
        #print(dot_cmd)
        sbp.run(dot_cmd, shell=True, stdout=sbp.PIPE, stderr=sbp.PIPE)
    
    return

def printTable1(myDict, colList=None):
    if not colList:
        colList = list(myDict[0].keys() if myDict else [])
    myList = [colList]

    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])

    colSize = [max(map(len, col)) for col in zip(*myList)]
    formatStr = ' | '.join(["{{:^{}}}".format(i) for i in colSize])
    myList.insert(1, ['-' * i for i in colSize])

    content = ''

    for item in myList:
        content = content + formatStr.format(*item) + '\n'

    return content

def printTable2(myDict, colList=None):
    #if not colList:
    #    colList = list(myDict[0].keys() if myDict else [])
    #myList = [colList]
    myList = []

    for key in myDict.keys():
        if type(myDict[key]) is list:
            list_ = [str(x) for x in myDict[key]]
            myList.append([key] + list_)
        else:
            myList.append([key, str(myDict[key])])

    colSize = [max(map(len, col)) for col in zip(*myList)]
    #formatStr = '\t '.join(["{{:^{}}}".format(i) for i in colSize])
    formatStr = '\t '.join(["{{:^{}}}".format(i) for i in range(len(colSize))])
    #myList.insert(1, ['-' * i for i in colSize])

    content = ''

    for item in myList:
        content = content + formatStr.format(*item) + '\n'

    return content
