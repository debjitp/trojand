#===============================================================================
#
#         FILE: logic_map_RS232.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Wed 13 Nov 2019 05:57:12 PM EST
#===============================================================================

import os, sys

LOGICS_90 = {
        'AND2X4': {'IN': ['IN1', 'IN2'], 'OUT': ['Q']},
        'ISOLORX8': {'IN': ['D', 'ISO'], 'OUT': ['Q']},
        'NAND4X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'OR4X4': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['Q']},
        'XOR2X2': {'IN': ['IN1', 'IN2'], 'OUT': ['Q']},
        'NOR2X4': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'INVX32': {'IN': ['IN'], 'OUT': ['QN']},
        'INVX0': {'IN': ['IN'], 'OUT': ['QN']},
        'NAND2X4': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'AOI21X2': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'AOI22X2': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'NAND3X4': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'OAI21X2': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'DFFARX1': {'IN': ['D', 'CLK', 'RSTB'], 'OUT': ['Q', 'QN']},
        'DFFASX1': {'IN': ['D', 'CLK', 'SETB'], 'OUT': ['Q', 'QN']},
        'MUX21X2': {'IN': ['IN1', 'IN2', 'S'], 'OUT': ['Q']},
        'MUX21X1': {'IN': ['IN1', 'IN2', 'S'], 'OUT': ['Q']},
        'NBUFFX16': {'IN': ['IN'], 'OUT': ['Q']}
        }

LOGICS_180 = {
        'OAI21X1': {'IN': ['A0', 'A1', 'B0'], 'OUT': ['Y']},
        'AOI21X1': {'IN': ['A0', 'A1', 'B0'], 'OUT': ['Y']},
        'INVX1': {'IN': ['A'], 'OUT': ['Y']},
        'AOI22X1': {'IN': ['A0', 'A1', 'B0', 'B1'], 'OUT': ['Y']},
        'NOR2X1': {'IN':['A', 'B'], 'OUT': ['Y']},
        'NAND3X1': {'IN': ['A', 'B', 'C'], 'OUT': ['Y']},
        'OR4X1': {'IN': ['A', 'B', 'C', 'D'], 'OUT': ['Y']},
        'AND2X1': {'IN': ['A', 'B'], 'OUT': ['Y']},
        'NAND4X1': {'IN': ['A', 'B', 'C', 'D'], 'OUT': ['Y']},
        'OR2X1': {'IN': ['A', 'B'], 'OUT': ['Y']},
        'NAND2X1': {'IN': ['A', 'B'], 'OUT': ['Y']},
        'XOR2X1': {'IN': ['A', 'B'], 'OUT': ['Y']},
        'SDFFSRX1': {'IN': ['D', 'SI', 'SE', 'CK', 'SN', 'RN'], 'OUT': ['Q', 'QN']},
        'MX2X1': {'IN': ['A', 'B', 'S0'], 'OUT': ['Y']},
        'BUFX1': {'IN': ['A'], 'OUT': ['Y']}
        }
