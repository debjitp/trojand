#===============================================================================
#
#         FILE: logic_map_s15850_s35932_s38417_s38584.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Tue 12 Nov 2019 10:24:51 PM EST
#===============================================================================

import os, sys

LOGICS = {
        'LSDNENX1': {'IN': ['D', 'ENB'], 'OUT': ['Q']},
        'SDFFX1': {'IN': ['D', 'SI', 'SE', 'CLK'], 'OUT': ['Q', 'QN']},
        'DFFX2': {'IN': ['CLK', 'D'], 'OUT': ['Q']},
        'DFFX2': {'IN': ['CLK', 'D'], 'OUT': ['Q']},
        'LSDNX1': {'IN': ['D'], 'OUT': ['Q']},
        'NAND2X0': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'NAND3X0': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'AO222X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5', 'IN6'], 'OUT': ['Q']},
        'AND4X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['Q']},
        'AND2X1': {'IN': ['IN1', 'IN2'], 'OUT': ['Q']},
        'AND3X4': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'NAND4X0': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'NAND3X4': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'NAND3X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'NAND2X1': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'AOI222X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5', 'IN6'], 'OUT': ['QN']},
        'INVX0': {'IN': ['IN'], 'OUT': ['QN']},
        'INVX8': {'IN': ['IN'], 'OUT': ['QN']},
        'AO22X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['Q']},
        'AO221X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5'], 'OUT': ['Q']},
        'AOI22X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'OA22X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': []},
        'ISOLANDX1': {'IN': ['D', 'ISO'], 'OUT': ['Q']},
        'NOR2X0': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'NOR2X2': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'NOR4X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'NAND4X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'XOR3X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'XNOR3X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'XNOR2X1': {'IN': ['IN1', 'IN2'], 'OUT': ['Q']},
        'NOR4X0': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'AO21X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'AND3X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'XOR2X1': {'IN': ['IN1', 'IN2'], 'OUT': ['Q']},
        'OA21X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'OA221X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5'], 'OUT': ['Q']},
        'OA222X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5', 'IN6'], 'OUT': ['Q']},
        'OR2X1': {'IN': ['IN1', 'IN2'], 'OUT': ['Q']},
        'NOR3X0': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'NOR3X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'NOR2X0': {'IN': ['IN1', 'IN2'], 'OUT': ['QN']},
        'NBUFFX2': {'IN': ['IN'], 'OUT': ['Q']},
        'OAI21X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'AOI221X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5'], 'OUT': ['QN']},
        'OAI221X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5'], 'OUT': ['QN']},
        'OAI222X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5', 'IN6'], 'OUT': ['QN']},
        'OAI22X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['QN']},
        'OR4X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4'], 'OUT': ['Q']},
        'AOI21X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['QN']},
        'OR3X1': {'IN': ['IN1', 'IN2', 'IN3'], 'OUT': ['Q']},
        'OA222X1': {'IN': ['IN1', 'IN2', 'IN3', 'IN4', 'IN5', 'IN6'], 'OUT': ['Q']},
        'MUX21X1': {'IN': ['IN1', 'IN2', 'S'], 'OUT': ['Q']},
        'MUX21X2': {'IN': ['IN1', 'IN2', 'S'], 'OUT': ['Q']}
        }
