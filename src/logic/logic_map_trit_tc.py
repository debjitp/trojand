#===============================================================================
#
#         FILE: logic_map_trit_tc.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-14-2019
#     REVISION: 
#    LMODIFIED: Wed 20 Nov 2019 02:32:30 PM EST
#===============================================================================

import os, sys

LOGICS = {
        'nor2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'nor3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'nor4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'nor2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'nor3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'nor4s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'nor5s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},#
        'nor5s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},#
        'nor6s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},#
        'nor6s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},#
        'hi1s1': {'IN': ['DIN'], 'OUT': ['Q']},#
        'nnd2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'nnd2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'nnd2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'nnd3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'nnd3s2': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'nnd3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'nnd5s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},#
        'nnd4s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'nnd4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'nnd4s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'nb1s1': {'IN': ['DIN'], 'OUT': ['Q']},#
        'and2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'and2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'and3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'and3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'and4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'and4s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'and5s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},#
        'and9s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6', 'DIN7', 'DIN8', 'DIN9'], 'OUT': ['Q']},#
        'or2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'or3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'or4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'or4s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},#
        'or5s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},#
        'or5s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},#
        'or2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'or3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},#
        'xor2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'xor2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'xnr2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},#
        'i1s1': {'IN': ['DIN'], 'OUT': ['Q']},#
        'i1s3': {'IN': ['DIN'], 'OUT': ['Q']},#
        'i1s12': {'IN': ['DIN'], 'OUT': ['Q']},#
        'i1s11': {'IN': ['DIN'], 'OUT': ['Q']},#
        'ib1s9': {'IN': ['DIN'], 'OUT': ['Q']},#
        'ib1s5': {'IN': ['DIN'], 'OUT': ['Q']},#
        'ib1s1': {'IN': ['DIN'], 'OUT': ['Q']},#
        'ib1s9': {'IN': ['DIN'], 'OUT': ['Q']},
        'sdffs1': {'IN': ['DIN', 'SDIN', 'SSEL', 'CLK'], 'OUT': ['Q']},#
        }
