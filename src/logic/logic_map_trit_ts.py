#===============================================================================
#
#         FILE: logic_map_trit_ts.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground truth dictionary
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 03-12-2019
#     REVISION: 
#    LMODIFIED: Wed 04 Dec 2019 02:55:50 AM EST
#===============================================================================

import os, sys

LOGICS = {'and2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'and2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'and2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'and3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'and3s2': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'and3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'and4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'and4s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoai1112s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'aoai122s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'aoi123s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'aoi13s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoi13s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoi211s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoi211s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoi21s2': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'aoi21s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'aoi221s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'aoi222s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'aoi22s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoi22s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'aoi23s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'aoi33s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'aoi42s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'aoi42s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'dffcs2': {'IN': ['CLRB', 'DIN', 'CLK'], 'OUT': ['Q']},
 'dffles2': {'IN': ['DIN', 'EB', 'CLK'], 'OUT': ['QN']},
 'dffs2': {'IN': ['DIN', 'CLK'], 'OUT': ['Q']},
 'dsmxc31s2': {'IN': ['DIN1', 'DIN2', 'CLK'], 'OUT': ['Q']},
 'hi1s1': {'IN': ['DIN'], 'OUT': ['Q']},
 'i1s1': {'IN': ['DIN'], 'OUT': ['Q']},
 'i1s11': {'IN': ['DIN'], 'OUT': ['Q']},
 'i1s12': {'IN': ['DIN'], 'OUT': ['Q']},
 'i1s3': {'IN': ['DIN'], 'OUT': ['Q']},
 'ib1s1': {'IN': ['DIN'], 'OUT': ['Q']},
 'ib1s2': {'IN': ['DIN'], 'OUT': ['Q']},
 'ib1s5': {'IN': ['DIN'], 'OUT': ['Q']},
 'ib1s9': {'IN': ['DIN'], 'OUT': ['Q']},
 'mx21s3': {'IN': ['DIN1', 'DIN2', 'SIN'], 'OUT': ['Q']},
 'mx41s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'SIN0', 'SIN1'], 'OUT': ['Q']},
 'mx41s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'SIN0', 'SIN1'], 'OUT': ['Q']},
 'mxi21s1': {'IN': ['DIN1', 'DIN2', 'SIN'], 'OUT': ['Q']},
 'mxi21s2': {'IN': ['DIN1', 'DIN2', 'SIN'], 'OUT': ['Q']},
 'mxi21s3': {'IN': ['DIN1', 'DIN2', 'SIN'], 'OUT': ['Q']},
 'mxi41s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'SIN0', 'SIN1'], 'OUT': ['Q']},
 'mxi41s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'SIN0', 'SIN1'], 'OUT': ['Q']},
 'nnd2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'nnd2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'nnd2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'nnd3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'nnd3s2': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'nnd3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'nnd4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'nnd4s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'nnd4s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'nnd5s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'nor2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'nor2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'nor2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'nor3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'nor3s2': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'nor3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'nor4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'nor4s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'nor5s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'nor5s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'nor6s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'nor6s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'oai1112s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'oai1112s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'oai13s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'oai211s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'oai211s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'oai21s2': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'oai21s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'oai221s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'oai222s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'oai22s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'oai22s2': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'oai24s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'oai321s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'oai322s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6', 'DIN7'], 'OUT': ['Q']},
 'oai32s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'oai33s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': []},
 'oai33s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5', 'DIN6'], 'OUT': ['Q']},
 'or2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'or2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'or2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'or3s1': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'or3s3': {'IN': ['DIN1', 'DIN2', 'DIN3'], 'OUT': ['Q']},
 'or4s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'or4s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4'], 'OUT': ['Q']},
 'or5s1': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'or5s3': {'IN': ['DIN1', 'DIN2', 'DIN3', 'DIN4', 'DIN5'], 'OUT': ['Q']},
 'sdffs1': {'IN': ['DIN', 'SDIN', 'SSEL', 'CLK'], 'OUT': ['Q', 'QN']},
 'xnr2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'xnr2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'xnr2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'xor2s1': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'xor2s2': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']},
 'xor2s3': {'IN': ['DIN1', 'DIN2'], 'OUT': ['Q']}}
