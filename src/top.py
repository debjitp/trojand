#===============================================================================
#
#         FILE: top.py
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 06-11-2019
#     REVISION: 
#    LMODIFIED: Fri 22 Nov 2019 09:21:59 PM EST
#===============================================================================

import os, sys
from argparse import ArgumentParser
from core.netlist_to_graph import main

if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-f', '--file', help='Netlist file with absolute path', \
            dest='floc', required=True)
    parser.add_argument('-g', '--graph', help='Graph file with absolute path', \
            dest='gloc', required=True)
    parser.add_argument('-d', '--design', help='Design name', \
            dest='design', required=True)
    parser.add_argument('-c', '--category', help='Design category', \
            dest='category', required=True)
    parser.add_argument('-s', '--suite', help='Design suite name', \
            dest='suite', default='')
    parser.add_argument('-l', '--learning', help='Graph learning file with absolute path', \
            dest='lloc', default='')
    parser.add_argument('-p', '--plot_graph', action='store_true', \
            help='Flag to control graph plotting', default=False, dest='plot')
    options = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        exit(0)

    main(options.floc, options.gloc, options.lloc, options.suite, options.design, options.category, \
            options.plot)
