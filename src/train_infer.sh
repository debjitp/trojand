#!/bin/bash

epoch_array=(5 10 15 20 25 30)
bsize_array=(256 512 1024 2048 3072)
chunk_array=(512)

for epoch in "${epoch_array[@]}"
do
    for bsize in "${bsize_array[@]}"
    do
        for csize in "${chunk_array[@]}"
        do
            echo -e "python model.py -e "$epoch" -b "$bsize" -c "$csize
            echo -e "\n"
            python model.py -e $epoch -b $bsize -c $csize
            echo -e "\n"
        done
    done
done
