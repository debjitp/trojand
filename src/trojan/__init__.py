#===============================================================================
#
#         FILE: __init__.py
#
#        USAGE:
#
#  DESCRIPTION: This script takes a Synopsys DC netlist and converts into a Synopsys
#               DC netlist by removing the newlines and extra white spaces
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Debjit Pal (debjit.pal@cornell.edu)
# ORGANIZATION: Cornell University
#      VERSION: 1.0
#      CREATED: 11/05/19
#     REVISION: ---
#===============================================================================
