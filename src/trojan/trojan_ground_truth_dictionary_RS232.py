#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_RS232.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Wed 06 Nov 2019 08:48:07 PM EST
#===============================================================================

import os, sys

trojan_RS232 = {
        'T1000':{
            'Trigger': ['U293', 'U294', 'U295', 'U296', 'U297', 'U298', 'U299', 'U300', 'U301', 'U302'],
            'Payload': ['U303', 'U304', 'U305']
            },
        'T1100':{
            'Trigger': ['iDatasend_reg_2_', 'U293', 'U294', 'U295', 'U296', 'U297', 'U298', 'U299', 'U300', 'U301', 'U302'],
            'Payload': ['U305']
            },
        'T1200':{
            'Trigger': ['iDatasend_reg_1', 'iDatasend_reg_2', 'iDatasend_reg_3', 'iDatasend_reg_4', 'U292', 'U293', 'U294', 'U295', 'U296', 'U297', 'U300', 'U301', 'U302'],
            'Payload': ['U303']
            },
        'T1300':{
            'Trigger': ['U292', 'U293', 'U294', 'U295', 'U296', 'U297', 'U302'],
            'Payload': ['U303', 'U304']
            },
        'T1400':{
            'Trigger': ['iDatasend_reg', 'U292', 'U293', 'U294', 'U295', 'U296', 'U297', 'U298', 'U299', 'U300', 'U301', 'U302'],
            'Payload': ['U303']
            },
        'T1500':{
            'Trigger': ['iDatasend_reg_2_', 'U293', 'U294', 'U295', 'U296', 'U297', 'U298', 'U299', 'U300', 'U301', 'U302'],
            'Payload': ['U303', 'U304', 'U305']
            },
        'T1600':{
            'Trigger': ['iDatasend_reg_1', 'iDatasend_reg_2', 'U293', 'U294', 'U295', 'U296', 'U297', 'U300', 'U301', 'U302'],
            'Payload': ['U303', 'U304']
            }
        }
