#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_s15850.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Tue 05 Nov 2019 06:49:34 PM EST
#===============================================================================

import os, sys

trojan_s15850 = {
        'T100': {
            'Trigger': ['Tg1_Trojan1', 'Tg1_Trojan2', 'Tg1_Trojan3', 'Tg1_Trojan4', \
                        'Tg1_Trojan1234', 'Tg1_Trojan5', 'Tg1_Trojan6', 'Tg1_Trojan7', \
                        'Tg1_Trojan8', 'Tg1_Trojan5678', 'Tg1_Tj_Trigger', 'Tg1_Trigger', \
                        'INVtest_se', 'Trojan_Trigger'],
            'Payload': ['Trojan_Paylaod']

            }
        }
