#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_s35932.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Tue 05 Nov 2019 06:49:45 PM EST
#===============================================================================

import os, sys

trojan_s35932 = {
        'T100': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234_NOT', \
                    'Trojan5', 'Trojan6', 'Trojan7', 'Trojan8', 'Trojan5678_NOT', \
                    'INV_test_se', 'Trojan_Trigger', 'TrojanScanEnable'],
            'Payload': ['Trojan_Paylaod1', 'Trojan_Paylaod2']
            },
        'T200': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234_NOT', \
                    'Trojan5', 'Trojan6', 'Trojan7', 'Trojan8', 'Trojan5678_NOT', \
                    'INVtest_se', 'Trojan_Trigger'],
            'Payload': ['U5548', 'U6802', 'U6740', 'U5566']
            },
        'T300': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234_NOT', \
                    'Trojan5', 'Trojan6', 'Trojan7', 'Trojan8', 'Trojan5678_NOT', \
                    'INVtest_se', 'Trojan_Trigger'],
            'Payload': ['TjPayload1', 'TjPayload2', 'TjPayload3', 'TjPayload4', 'TjPayload5', \
                    'TjPayload6', \
                    'TjPayload7', 'TjPayload8', 'TjPayload9', 'TjPayload10', 'TjPayload11', \
                    'TjPayload12', \
                    'TjPayload13', 'TjPayload14', 'TjPayload15', 'TjPayload16', 'TjPayload17', \
                    'TjPayload18', \
                    'TjPayload19', 'TjPayload20', 'TjPayload21', 'TjPayload22', 'TjPayload23', \
                    'TjPayload24']
            }
        }
