#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_s38417.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Tue 05 Nov 2019 06:49:55 PM EST
#===============================================================================

import os, sys

trojan_s38417 = {
        'T100': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234_NOT', \
                    'Trojan5', 'Trojan6', 'Trojan7', 'Trojan8', 'Trojan5678_NOT', \
                    'Trojan_CLK_NOT'],
            'Payload': ['Trojan_Payload']
            },
        'T200': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234', \
                    'Trojan5', 'Trojan6', 'Trojan7', 'Trojan8', 'Trojan5678', \
                    'Trojan_Trigger'],
            'Payload': ['Trojan_Paylaod_1', 'Trojan_Paylaod_2', \
                    'Trojan_Paylaod_3', 'Trojan_Paylaod_4']
            },
        'T300': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234_NOT', \
                    'Trojan5', 'Trojan6', 'Trojan7', 'Trojan8', 'Trojan5678_NOT', \
                    'Trojan_CLK_NOT'],
            'Payload': ['Trojan_Paylaod1', 'Trojan_Paylaod2', 'Trojan_Paylaod3', \
                    'Trojan_Payload', 'TrojanEnableGATE', \
                    'Ring_Inv1', 'Ring_Inv2', 'Ring_Inv3', 'Ring_Inv4', 'Ring_Inv5', \
                    'Ring_Inv6', 'Ring_Inv7', 'Ring_Inv8', 'Ring_Inv9', 'Ring_Inv10', \
                    'Ring_Inv11', 'Ring_Inv12', 'Ring_Inv13', 'Ring_Inv14', 'Ring_Inv15', \
                    'Ring_Inv16', 'Ring_Inv17', 'Ring_Inv18', 'Ring_Inv19', 'Ring_Inv20', \
                    'Ring_Inv21', 'Ring_Inv22', 'Ring_Inv23', 'Ring_Inv24', 'Ring_Inv25', \
                    'Ring_Inv26', 'Ring_Inv27', 'Ring_Inv28']
            }
        }
