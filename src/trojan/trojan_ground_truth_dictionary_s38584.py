#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_38584.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-11-2019
#     REVISION: 
#    LMODIFIED: Tue 05 Nov 2019 06:50:04 PM EST
#===============================================================================

import os, sys

trojan_s38584 = {
        'T100': {
            'Trigger': ['Trojan1', 'Trojan2', 'Trojan3', 'Trojan4', 'Trojan1234_NOT', \
                    'Trojan5', 'NOT_test_se', 'Trojan_Trigger'],
            'Payload': ['Trojan_Payload']
            },
        'T200': {
            'Trigger': [],
            'Payload': []
            },
        'T300': {
            'Trigger': [],
            'Payload': []
            }
        }
