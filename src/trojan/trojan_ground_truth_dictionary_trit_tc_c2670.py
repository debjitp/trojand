#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_trit_tc_c2670.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-15-2019
#     REVISION: 
#    LMODIFIED: Sat 16 Nov 2019 11:10:29 PM EST
#===============================================================================

import os, sys

trojan_trit_tc_c2670 = {
        'T000':{
            'Trigger': ['troj0_0U1', 'troj0_0U2', 'troj0_0U3', 'troj0_0U4'],
            'Payload': ['trojan0_0']
            },
        'T001':{
            'Trigger': ['troj1_0U1', 'troj1_0U2', 'troj1_0U3', 'troj1_0U4', 'troj1_0U5', 'troj1_0U6'],
            'Payload': ['trojan1_0']
            },
        'T002':{
            'Trigger': ['troj2_0U1', 'troj2_0U2', 'troj2_0U3', 'troj2_0U4'],
            'Payload': ['trojan2_0']
            },
        'T003':{
            'Trigger': ['troj3_0U1', 'troj3_0U2', 'troj3_0U3', 'troj3_0U4', 'troj3_0U5', 'troj3_0U6'],
            'Payload': ['trojan3_0']
            },
        'T004':{
            'Trigger': ['troj4_0U1', 'troj4_0U2', 'troj4_0U3', 'troj4_0U4', 'troj4_0U5', 'troj4_0U6', \
                    'troj4_0U7', 'troj4_0U8', 'troj4_0U9'],
            'Payload': ['trojan4_0']
            },
        'T005':{
            'Trigger': ['troj5_0U1', 'troj5_0U2', 'troj5_0U3', 'troj5_0U4', 'troj5_0U5', 'troj5_0U6', \
                    'troj5_0U7', 'troj5_0U8', 'troj5_0U9'],
            'Payload': ['trojan5_0']
            },
        'T006':{
            'Trigger': ['troj6_0U1', 'troj6_0U2', 'troj6_0U3', 'troj6_0U4', 'troj6_0U5', 'troj6_0U6'],
            'Payload': ['trojan6_0']
            },
        'T007':{
            'Trigger': ['troj7_0U1', 'troj7_0U2', 'troj7_0U3', 'troj7_0U4', 'troj7_0U5', 'troj7_0U6', \
                    'troj7_0U7'],
            'Payload': ['trojan7_0']
            },
        'T008':{
            'Trigger': ['troj8_0U1', 'troj8_0U2', 'troj8_0U3', 'troj8_0U4', 'troj8_0U5', 'troj8_0U6', \
                    'troj8_0U7'],
            'Payload': ['trojan8_0']
            },
        'T009':{
            'Trigger': ['troj9_0U1', 'troj9_0U2', 'troj9_0U3'],
            'Payload': ['trojan9_0']
            },
        'T010':{
            'Trigger': ['troj10_0U1', 'troj10_0U2', 'troj10_0U3', 'troj10_0U4', 'troj10_0U5', 'troj10_0U6'],
            'Payload': ['trojan10_0']
            },
        'T011':{
            'Trigger': ['troj11_0U1', 'troj11_0U2', 'troj11_0U3', 'troj11_0U4', 'troj11_0U5', \
                    'troj11_0U6', 'troj11_0U7', 'troj11_0U8', 'troj11_0U9'],
            'Payload': ['trojan11_0']
            },
        'T012':{
            'Trigger': ['troj12_0U1', 'troj12_0U2', 'troj12_0U3'],
            'Payload': ['trojan12_0']
            },
        'T013':{
            'Trigger': ['troj13_0U1', 'troj13_0U2', 'troj13_0U3', 'troj13_0U4', 'troj13_0U5', \
                    'troj13_0U6', 'troj13_0U7'],
            'Payload': ['trojan13_0']
            },
        'T014':{
            'Trigger': ['troj14_0U1', 'troj14_0U2', 'troj14_0U3', 'troj14_0U4', 'troj14_0U5', \
                    'troj14_0U6'],
            'Payload': ['trojan14_0']
            },
        'T015':{
            'Trigger': ['troj15_0U1', 'troj15_0U2', 'troj15_0U3', 'troj15_0U4', 'troj15_0U5', \
                    'troj15_0U6', 'troj15_0U7', 'troj15_0U8', 'troj15_0U9', 'troj15_0U10', \
                    'troj15_0U11'],
            'Payload': ['trojan15_0']
            },
        'T016':{
            'Trigger': ['troj16_0U1', 'troj16_0U2', 'troj16_0U3', 'troj16_0U4', 'troj16_0U5', 'troj16_0U6'],
            'Payload': ['trojan16_0']
            },
        'T017':{
            'Trigger': ['troj17_0U1', 'troj17_0U2', 'troj17_0U3', 'troj17_0U4', 'troj17_0U5', \
                    'troj17_0U6', 'troj17_0U7', 'troj17_0U8', 'troj17_0U9'],
            'Payload': ['trojan17_0']
            },
        'T018':{
            'Trigger': ['troj18_0U1', 'troj18_0U2', 'troj18_0U3', 'troj18_0U4', 'troj18_0U5', 'troj18_0U6'],
            'Payload': ['trojan18_0']
            },
        'T019':{
            'Trigger': ['troj19_0U1', 'troj19_0U2', 'troj19_0U3', 'troj19_0U4', 'troj19_0U5', 'troj19_0U6'],
            'Payload': ['trojan19_0']
            },
        'T020':{
            'Trigger': ['troj20_0U1', 'troj20_0U2', 'troj20_0U3', 'troj20_0U4', 'troj20_0U5', \
                    'troj20_0U6', 'troj20_0U7'],
            'Payload': ['trojan20_0']
            },
        'T021':{
            'Trigger': ['troj21_0U1', 'troj21_0U2', 'troj21_0U3', 'troj21_0U4', 'troj21_0U5', 'troj21_0U6'],
            'Payload': ['trojan21_0']
            },
        'T022':{
            'Trigger': ['troj22_0U1', 'troj22_0U2', 'troj22_0U3', 'troj22_0U4', 'troj22_0U5', \
                    'troj22_0U6', 'troj22_0U7', 'troj22_0U8', 'troj22_0U9'],
            'Payload': ['trojan22_0']
            },
        'T023':{
            'Trigger': ['troj23_0U1', 'troj23_0U2', 'troj23_0U3', 'troj23_0U4', 'troj23_0U5', \
                    'troj23_0U6', 'troj23_0U7', 'troj23_0U8'],
            'Payload': ['trojan23_0']
            },
        'T024':{
            'Trigger': ['troj24_0U1', 'troj24_0U2', 'troj24_0U3', 'troj24_0U4', 'troj24_0U5', 'troj24_0U6'],
            'Payload': ['trojan24_0']
            },
        'T025':{
            'Trigger': ['troj25_0U1', 'troj25_0U2', 'troj25_0U3', 'troj25_0U4', 'troj25_0U5'],
            'Payload': ['trojan25_0']
            },
        'T026':{
            'Trigger': ['troj26_0U1', 'troj26_0U2', 'troj26_0U3', 'troj26_0U4', 'troj26_0U5', \
                    'troj26_0U6', 'troj26_0U7', 'troj26_0U8'],
            'Payload': ['trojan26_0']
            },
        'T027':{
            'Trigger': ['troj27_0U1', 'troj27_0U2', 'troj27_0U3', 'troj27_0U4', 'troj27_0U5'],
            'Payload': ['trojan27_0']
            },
        'T028':{
            'Trigger': ['troj28_0U1', 'troj28_0U2', 'troj28_0U3', 'troj28_0U4', 'troj28_0U5', \
                    'troj28_0U6'],
            'Payload': ['trojan28_0']
            },
        'T029':{
            'Trigger': ['troj29_0U1', 'troj29_0U2', 'troj29_0U3', 'troj29_0U4', 'troj29_0U5', \
                    'troj29_0U6'],
            'Payload': ['trojan29_0']
            },
        'T030':{
            'Trigger': ['troj30_0U1', 'troj30_0U2', 'troj30_0U3', 'troj30_0U4', 'troj30_0U5'],
            'Payload': ['trojan30_0']
            },
        'T031':{
            'Trigger': ['troj31_0U1', 'troj31_0U2', 'troj31_0U3', 'troj31_0U4', 'troj31_0U5', \
                    'troj31_0U6', 'troj31_0U7', 'troj31_0U8'],
            'Payload': ['trojan31_0']
            },
        'T032':{
            'Trigger': ['troj32_0U1', 'troj32_0U2', 'troj32_0U3', 'troj32_0U4', 'troj32_0U5', \
                    'troj32_0U6'],
            'Payload': ['trojan32_0']
            },
        'T033':{
            'Trigger': ['troj33_0U1', 'troj33_0U2', 'troj33_0U3', 'troj33_0U4', 'troj33_0U5', \
                    'troj33_0U6'],
            'Payload': ['trojan33_0']
            },
        'T034':{
            'Trigger': ['troj34_0U1', 'troj34_0U2', 'troj34_0U3', 'troj34_0U4', 'troj34_0U5'],
            'Payload': ['trojan34_0']
            },
        'T035':{
            'Trigger': ['troj35_0U1', 'troj35_0U2', 'troj35_0U3', 'troj35_0U4'],
            'Payload': ['trojan35_0']
            },
        'T036':{
            'Trigger': ['troj36_0U1', 'troj36_0U2', 'troj36_0U3'],
            'Payload': ['trojan36_0']
            },
        'T037':{
            'Trigger': ['troj37_0U1', 'troj37_0U2', 'troj37_0U3', 'troj37_0U4'],
            'Payload': ['trojan37_0']
            },
        'T038':{
            'Trigger': ['troj38_0U1', 'troj38_0U2', 'troj38_0U3', 'troj38_0U4'],
            'Payload': ['trojan38_0']
            },
        'T039':{
            'Trigger': ['troj39_0U1', 'troj39_0U2', 'troj39_0U3', 'troj39_0U4', 'troj39_0U5', \
                    'troj39_0U6', 'troj39_0U7', 'troj39_0U8', 'troj39_0U9'],
            'Payload': ['trojan39_0']
            },
        'T040':{
            'Trigger': ['troj40_0U1', 'troj40_0U2', 'troj40_0U3', 'troj40_0U4', 'troj40_0U5', \
                    'troj40_0U6', 'troj40_0U7'],
            'Payload': ['trojan40_0']
            },
        'T041':{
            'Trigger': ['troj41_0U1', 'troj41_0U2', 'troj41_0U3', 'troj41_0U4'],
            'Payload': ['trojan41_0']
            },
        'T042':{
            'Trigger': ['troj42_0U1', 'troj42_0U2', 'troj42_0U3', 'troj42_0U4', 'troj42_0U5', \
                    'troj42_0U6'],
            'Payload': ['trojan42_0']
            },
        'T043':{
            'Trigger': ['troj43_0U1', 'troj43_0U2', 'troj43_0U3', 'troj43_0U4', 'troj43_0U5', \
                    'troj43_0U6', 'troj43_0U7', 'troj43_0U8'],
            'Payload': ['trojan43_0']
            },
        'T044':{
            'Trigger': ['troj44_0U1', 'troj44_0U2', 'troj44_0U3', 'troj44_0U4', 'troj44_0U5'],
            'Payload': ['trojan44_0']
            },
        'T045':{
            'Trigger': ['troj45_0U1', 'troj45_0U2', 'troj45_0U3', 'troj45_0U4', 'troj45_0U5', \
                    'troj45_0U6'],
            'Payload': ['trojan45_0']
            },
        'T046':{
            'Trigger': ['troj46_0U1', 'troj46_0U2', 'troj46_0U3', 'troj46_0U4', 'troj46_0U5', \
                    'troj46_0U6', 'troj46_0U7'],
            'Payload': ['trojan46_0']
            },
        'T047':{
            'Trigger': ['troj47_0U1', 'troj47_0U2', 'troj47_0U3', 'troj47_0U4', 'troj47_0U5'],
            'Payload': ['trojan47_0']
            },
        'T048':{
            'Trigger': ['troj48_0U1', 'troj48_0U2', 'troj48_0U3', 'troj48_0U4', 'troj48_0U5', \
                    'troj48_0U6'],
            'Payload': ['trojan48_0']
            },
        'T049':{
            'Trigger': ['troj49_0U1', 'troj49_0U2', 'troj49_0U3', 'troj49_0U4', 'troj49_0U5'],
            'Payload': ['trojan49_0']
            },
        'T050':{
            'Trigger': ['troj50_0U1', 'troj50_0U2', 'troj50_0U3', 'troj50_0U4', 'troj50_0U5', \
                    'troj50_0U6'],
            'Payload': ['trojan50_0']
            },
        'T051':{
            'Trigger': ['troj51_0U1', 'troj51_0U2', 'troj51_0U3', 'troj51_0U4', 'troj51_0U5', \
                    'troj51_0U6', 'troj51_0U7'],
            'Payload': ['trojan51_0']
            },
        'T052':{
            'Trigger': ['troj52_0U1', 'troj52_0U2', 'troj52_0U3', 'troj52_0U4', 'troj52_0U5', \
                    'troj52_0U6'],
            'Payload': ['trojan52_0']
            },
        'T053':{
            'Trigger': ['troj53_0U1', 'troj53_0U2', 'troj53_0U3', 'troj53_0U4', 'troj53_0U5', \
                    'troj53_0U6'],
            'Payload': ['trojan53_0']
            },
        'T054':{
            'Trigger': ['troj54_0U1', 'troj54_0U2', 'troj54_0U3', 'troj54_0U4', 'troj54_0U5'],
            'Payload': ['trojan54_0']
            },
        'T055':{
            'Trigger': ['troj55_0U1', 'troj55_0U2', 'troj55_0U3'],
            'Payload': ['trojan55_0']
            },
        'T056':{
            'Trigger': ['troj56_0U1', 'troj56_0U2', 'troj56_0U3', 'troj56_0U4', 'troj56_0U5'],
            'Payload': ['trojan56_0']
            },
        'T057':{
            'Trigger': ['troj57_0U1', 'troj57_0U2', 'troj57_0U3', 'troj57_0U4', 'troj57_0U5'],
            'Payload': ['trojan57_0']
            },
        'T058':{
            'Trigger': ['troj58_0U1', 'troj58_0U2', 'troj58_0U3', 'troj58_0U4', 'troj58_0U5', \
                    'troj58_0U6', 'troj58_0U7'],
            'Payload': ['trojan58_0']
            },
        'T059':{
            'Trigger': ['troj59_0U1', 'troj59_0U2', 'troj59_0U3', 'troj59_0U4', 'troj59_0U5', \
                    'troj59_0U6', 'troj59_0U7'],
            'Payload': ['trojan59_0']
            },
        'T060':{
            'Trigger': ['troj60_0U1', 'troj60_0U2', 'troj60_0U3', 'troj60_0U4', 'troj60_0U5', \
                    'troj60_0U6', 'troj60_0U7', 'troj60_0U8'],
            'Payload': ['trojan60_0']
            },
        'T061':{
            'Trigger': ['troj61_0U1', 'troj61_0U2', 'troj61_0U3', 'troj61_0U4', 'troj61_0U5', \
                    'troj61_0U6', 'troj61_0U7', 'troj61_0U8'],
            'Payload': ['trojan61_0']
            },
        'T062':{
            'Trigger': ['troj62_0U1', 'troj62_0U2', 'troj62_0U3', 'troj62_0U4'],
            'Payload': ['trojan62_0']
            },
        'T063':{
            'Trigger': ['troj63_0U1', 'troj63_0U2', 'troj63_0U3', 'troj63_0U4', 'troj63_0U5', \
                    'troj63_0U6', 'troj63_0U7'],
            'Payload': ['trojan63_0']
            },
        'T064':{
            'Trigger': ['troj64_0U1', 'troj64_0U2', 'troj64_0U3', 'troj64_0U4', 'troj64_0U5', \
                    'troj64_0U6', 'troj64_0U7'],
            'Payload': ['trojan64_0']
            },
        'T065':{
            'Trigger': ['troj65_0U1', 'troj65_0U2', 'troj65_0U3', 'troj65_0U4', 'troj65_0U5'],
            'Payload': ['trojan65_0']
            },
        'T066':{
            'Trigger': ['troj66_0U1', 'troj66_0U2', 'troj66_0U3', 'troj66_0U4', 'troj66_0U5', \
                    'troj66_0U6'],
            'Payload': ['trojan66_0']
            },
        'T067':{
            'Trigger': ['troj67_0U1', 'troj67_0U2', 'troj67_0U3', 'troj67_0U4', 'troj67_0U5', \
                    'troj67_0U6'],
            'Payload': ['trojan67_0']
            },
        'T068':{
            'Trigger': ['troj68_0U1', 'troj68_0U2', 'troj68_0U3', 'troj68_0U4', 'troj68_0U5'],
            'Payload': ['trojan68_0']
            },
        'T069':{
            'Trigger': ['troj69_0U1', 'troj69_0U2', 'troj69_0U3', 'troj69_0U4', 'troj69_0U5', \
                    'troj69_0U6'],
            'Payload': ['trojan69_0']
            },
        'T070':{
            'Trigger': ['troj70_0U1', 'troj70_0U2', 'troj70_0U3', 'troj70_0U4'],
            'Payload': ['trojan70_0']
            },
        'T071':{
            'Trigger': ['troj71_0U1', 'troj71_0U2', 'troj71_0U3', 'troj71_0U4', 'troj71_0U5'],
            'Payload': ['trojan71_0']
            },
        'T072':{
            'Trigger': ['troj72_0U1', 'troj72_0U2', 'troj72_0U3', 'troj72_0U4', 'troj72_0U5'],
            'Payload': ['trojan72_0']
            },
        'T073':{
            'Trigger': ['troj73_0U1', 'troj73_0U2', 'troj73_0U3', 'troj73_0U4', 'troj73_0U5', \
                    'troj73_0U6', 'troj73_0U7'],
            'Payload': ['trojan73_0']
            },
        'T074':{
            'Trigger': ['troj74_0U1', 'troj74_0U2', 'troj74_0U3', 'troj74_0U4', 'troj74_0U5', \
                    'troj74_0U6'],
            'Payload': ['trojan74_0']
            },
        'T075':{
            'Trigger': ['troj75_0U1', 'troj75_0U2', 'troj75_0U3', 'troj75_0U4', 'troj75_0U5', \
                    'troj75_0U6'],
            'Payload': ['trojan75_0']
            },
        'T076':{
            'Trigger': ['troj76_0U1', 'troj76_0U2', 'troj76_0U3', 'troj76_0U4', 'troj76_0U5'],
            'Payload': ['trojan76_0']
            },
        'T077':{
            'Trigger': ['troj77_0U1', 'troj77_0U2', 'troj77_0U3', 'troj77_0U4', 'troj77_0U5', \
                    'troj77_0U6', 'troj77_0U7'],
            'Payload': ['trojan77_0']
            },
        'T078':{
            'Trigger': ['troj78_0U1', 'troj78_0U2', 'troj78_0U3', 'troj78_0U4', 'troj78_0U5', \
                    'troj78_0U6', 'troj78_0U7', 'troj78_0U8', 'troj78_0U9', \
                    'troj78_0U10', 'troj78_0U11', 'troj78_0U12', 'troj78_0U13'],
            'Payload': ['trojan78_0']
            },
        'T079':{
            'Trigger': ['troj79_0U1', 'troj79_0U2', 'troj79_0U3', 'troj79_0U4', 'troj79_0U5', \
                    'troj79_0U6', 'troj79_0U7'],
            'Payload': ['trojan79_0']
            },
        'T080':{
            'Trigger': ['troj80_0U1', 'troj80_0U2', 'troj80_0U3', 'troj80_0U4', 'troj80_0U5', \
                    'troj80_0U6', 'troj80_0U7', 'troj80_0U8', 'troj80_0U9', \
                    'troj80_0U10'],
            'Payload': ['trojan80_0']
            },
        'T081':{
            'Trigger': ['troj81_0U1', 'troj81_0U2', 'troj81_0U3', 'troj81_0U4', 'troj81_0U5', \
                    'troj81_0U6', 'troj81_0U7'],
            'Payload': ['trojan81_0']
            },
        'T082':{
            'Trigger': ['troj82_0U1', 'troj82_0U2', 'troj82_0U3', 'troj82_0U4', 'troj82_0U5', \
                    'troj82_0U6', 'troj82_0U7', 'troj82_0U8', 'troj82_0U9', \
                    'troj82_0U10', 'troj82_0U11', 'troj82_0U12'],
            'Payload': ['trojan82_0']
            },
        'T083':{
            'Trigger': ['troj83_0U1', 'troj83_0U2', 'troj83_0U3', 'troj83_0U4', 'troj83_0U5', \
                    'troj83_0U6', 'troj83_0U7', 'troj83_0U8'],
            'Payload': ['trojan83_0']
            },
        'T084':{
            'Trigger': ['troj84_0U1', 'troj84_0U2', 'troj84_0U3', 'troj84_0U4', 'troj84_0U5', \
                    'troj84_0U6', 'troj84_0U7', 'troj84_0U8', 'troj84_0U9'],
            'Payload': ['trojan84_0']
            },
        'T085':{
            'Trigger': ['troj85_0U1', 'troj85_0U2', 'troj85_0U3', 'troj85_0U4', 'troj85_0U5', \
                    'troj85_0U6', 'troj85_0U7'],
            'Payload': ['trojan85_0']
            },
        'T086':{
            'Trigger': ['troj86_0U1', 'troj86_0U2', 'troj86_0U3', 'troj86_0U4', 'troj86_0U5', \
                    'troj86_0U6'],
            'Payload': ['trojan86_0']
            },
        'T087':{
            'Trigger': ['troj87_0U1', 'troj87_0U2', 'troj87_0U3', 'troj87_0U4', 'troj87_0U5', \
                    'troj87_0U6', 'troj87_0U7'],
            'Payload': ['trojan87_0']
            },
        'T088':{
            'Trigger': ['troj88_0U1', 'troj88_0U2', 'troj88_0U3', 'troj88_0U4', 'troj88_0U5', \
                    'troj88_0U6'],
            'Payload': ['trojan88_0']
            },
        'T089':{
            'Trigger': ['troj89_0U1', 'troj89_0U2', 'troj89_0U3', 'troj89_0U4', 'troj89_0U5'],
            'Payload': ['trojan89_0']
            },
        'T090':{
            'Trigger': ['troj90_0U1', 'troj90_0U2', 'troj90_0U3', 'troj90_0U4', 'troj90_0U5'],
            'Payload': ['trojan90_0']
            },
        'T091':{
            'Trigger': ['troj91_0U1', 'troj91_0U2', 'troj91_0U3', 'troj91_0U4', 'troj91_0U5'],
            'Payload': ['trojan91_0']
            },
        'T092':{
            'Trigger': ['troj92_0U1', 'troj92_0U2', 'troj92_0U3', 'troj92_0U4', 'troj92_0U5'],
            'Payload': ['trojan92_0']
            },
        'T093':{
            'Trigger': ['troj93_0U1', 'troj93_0U2', 'troj93_0U3', 'troj93_0U4', 'troj93_0U5', \
                    'troj93_0U6', 'troj93_0U7', 'troj93_0U8'],
            'Payload': ['trojan93_0']
            },
        'T094':{
            'Trigger': ['troj94_0U1', 'troj94_0U2', 'troj94_0U3', 'troj94_0U4', 'troj94_0U5'],
            'Payload': ['trojan94_0']
            },
        'T095':{
            'Trigger': ['troj95_0U1', 'troj95_0U2', 'troj95_0U3', 'troj95_0U4', 'troj95_0U5'],
            'Payload': ['trojan95_0']
            },
        'T096':{
            'Trigger': ['troj96_0U1', 'troj96_0U2', 'troj96_0U3', 'troj96_0U4'],
            'Payload': ['trojan96_0']
            },
        'T097':{
            'Trigger': ['troj97_0U1', 'troj97_0U2', 'troj97_0U3', 'troj97_0U4', 'troj97_0U5', \
                    'troj97_0U6'],
            'Payload': ['trojan97_0']
            },
        'T098':{
            'Trigger': ['troj98_0U1', 'troj98_0U2', 'troj98_0U3', 'troj98_0U4', 'troj98_0U5', \
                    'troj98_0U6', 'troj98_0U7'],
            'Payload': ['trojan98_0']
            },
        'T099':{
            'Trigger': ['troj99_0U1', 'troj99_0U2', 'troj99_0U3', 'troj99_0U4'],
            'Payload': ['trojan99_0']
            },

        }
