#===============================================================================
#
#         FILE: trojan_ground_truth_dictionary_trit_tc_s15850.py
#
#        USAGE:
#
#  DESCRIPTION: Trojan ground thruth dictionary for different gate 
#               gate level designs from Trust-Hub
#
#      OPTIONS: 
# REQUIREMENTS: 
#         BUGS: 
#        NOTES: 
#       AUTHOR: Debjit Pal
#      CONTACT: debjit.pal@cornell.edu
# ORGANIZATION: ECE, Cornell University
#      VERSION: 
#      CREATED: 05-16-2019
#     REVISION: 
#    LMODIFIED: Tue 19 Nov 2019 12:40:02 AM EST
#===============================================================================

import os, sys

trojan_trit_tc_s15850 = {
        'T000':{
            'Trigger': ['troj0_0U1', 'troj0_0U2', 'troj0_0U3', 'troj0_0U4', 'troj0_0U5'],
            'Payload': ['trojan0_0']
            },
        'T001':{
            'Trigger': ['troj1_0U1', 'troj1_0U2', 'troj1_0U3', 'troj1_0U4', 'troj1_0U5'],
            'Payload': ['trojan1_0']
            },
        'T002':{
            'Trigger': ['troj2_0U1', 'troj2_0U2', 'troj2_0U3', 'troj2_0U4', 'troj2_0U5', \
                    'troj2_0U6'],
            'Payload': ['trojan2_0']
            },
        'T003':{
            'Trigger': ['troj3_0U1', 'troj3_0U2', 'troj3_0U3', 'troj3_0U4', 'troj3_0U5', \
                    'troj3_0U6'],
            'Payload': ['trojan3_0']
            },
        'T004':{
            'Trigger': ['troj4_0U1', 'troj4_0U2', 'troj4_0U3', 'troj4_0U4', 'troj4_0U5'],
            'Payload': ['trojan4_0']
            },
        'T005':{
            'Trigger': ['troj5_0U1', 'troj5_0U2', 'troj5_0U3', 'troj5_0U4', 'troj5_0U5'],
            'Payload': ['trojan5_0']
            },
        'T006':{
            'Trigger': ['troj6_0U1', 'troj6_0U2', 'troj6_0U3', 'troj6_0U4', 'troj6_0U5', 'troj6_0U6'],
            'Payload': ['trojan6_0']
            },
        'T007':{
            'Trigger': ['troj7_0U1', 'troj7_0U2', 'troj7_0U3', 'troj7_0U4'],
            'Payload': ['trojan7_0']
            },
        'T008':{
            'Trigger': ['troj8_0U1', 'troj8_0U2', 'troj8_0U3', 'troj8_0U4', 'troj8_0U5'],
            'Payload': ['trojan8_0']
            },
        'T009':{
            'Trigger': ['troj9_0U1', 'troj9_0U2', 'troj9_0U3', 'troj9_0U4', \
                    'troj9_0U5', 'troj9_0U6', 'troj9_0U7'],
            'Payload': ['trojan9_0']
            },
        'T010':{
            'Trigger': ['troj10_0U1', 'troj10_0U2', 'troj10_0U3', 'troj10_0U4', \
                    'troj10_0U5', 'troj10_0U6', 'troj10_0U7'],
            'Payload': ['trojan10_0']
            },
        'T011':{
            'Trigger': ['troj11_0U1', 'troj11_0U2', 'troj11_0U3'],
            'Payload': ['trojan11_0']
            },
        'T012':{
            'Trigger': ['troj12_0U1', 'troj12_0U2', 'troj12_0U3', 'troj12_0U4', \
                    'troj12_0U5', 'troj12_0U6', 'troj12_0U7'],
            'Payload': ['trojan12_0']
            },
        'T013':{
            'Trigger': ['troj13_0U1', 'troj13_0U2', 'troj13_0U3', 'troj13_0U4', \
                    'troj13_0U5', 'troj13_0U6', 'troj13_0U7'],
            'Payload': ['trojan13_0']
            },
        'T014':{
            'Trigger': ['troj14_0U1', 'troj14_0U2', 'troj14_0U3'],
            'Payload': ['trojan14_0']
            },
        'T015':{
            'Trigger': ['troj15_0U1', 'troj15_0U2', 'troj15_0U3', 'troj15_0U4', 'troj15_0U5', \
                    'troj15_0U6', 'troj15_0U7', 'troj15_0U8', 'troj15_0U9'],
            'Payload': ['trojan15_0']
            },
        'T016':{
            'Trigger': ['troj16_0U1', 'troj16_0U2', 'troj16_0U3', 'troj16_0U4', 'troj16_0U5', \
                    'troj17_0U6', 'troj17_0U7'],
            'Payload': ['trojan16_0']
            },
        'T017':{
            'Trigger': ['troj17_0U1', 'troj17_0U2', 'troj17_0U3', 'troj17_0U4', 'troj17_0U5', \
                    'troj17_0U6', 'troj17_0U7'],
            'Payload': ['trojan17_0']
            },
        'T018':{
            'Trigger': ['troj18_0U1', 'troj18_0U2', 'troj18_0U3', 'troj18_0U4', 'troj18_0U5', \
                    'troj18_0U6', 'troj18_0U7', 'troj18_0U8', 'troj18_0U9', 'troj18_0U10'],
            'Payload': ['trojan18_0']
            },
        'T019':{
            'Trigger': ['troj19_0U1', 'troj19_0U2', 'troj19_0U3', 'troj19_0U4'],
            'Payload': ['trojan19_0']
            },
        ## Per design 2 Trojans
        'T200':{
            'Trigger': ['troj0_0U1', 'troj0_0U2', 'troj0_0U3', 'troj0_0U4', 'troj0_0U5', \
                    'troj0_1U1', 'troj0_1U2', 'troj0_1U3', 'troj0_1U4', 'troj0_1U5', 'troj0_1U6',\
                    'troj0_1U7', 'troj0_1U8'],
            'Payload': ['trojan0_0', 'trojan0_1']
            },
        'T201':{
            'Trigger': ['troj1_0U1', 'troj1_0U2', 'troj1_0U3', 'troj1_0U4', 'troj1_0U5', \
                    'troj1_1U1', 'troj1_1U2', 'troj1_1U3', 'troj1_1U4', 'troj1_1U5'],
            'Payload': ['trojan0_0', 'trojan1_1']
            },
        'T202':{
            'Trigger': ['troj2_0U1', 'troj2_0U2', 'troj2_0U3', 'troj2_0U4', 'troj2_0U5', \
                    'troj2_0U6', 'troj2_0U7',\
                    'troj2_1U1', 'troj2_1U2', 'troj2_1U3', 'troj2_1U4', 'troj2_1U5'],
            'Payload': ['trojan2_0', 'trojan2_1']
            },
        'T203':{
            'Trigger': ['troj3_0U1', 'troj3_0U2', 'troj3_0U3', 'troj3_0U4', \
                    'troj3_0U5', 'troj3_0U6', \
                    'troj3_1U1', 'troj3_1U2', 'troj3_1U3', 'troj3_1U4', \
                    'troj3_1U5', 'troj3_1U6', 'troj3_1U7', 'troj3_1U8'],
            'Payload': ['trojan3_0', 'trojan3_1']
            },
        'T204':{
            'Trigger': ['troj4_0U1', 'troj4_0U2', 'troj4_0U3', 'troj4_0U4', 'troj4_0U5', \
                    'troj4_0U6', \
                    'troj4_1U1', 'troj4_1U2', 'troj4_1U3', 'troj4_1U4', 'troj4_1U5', 'troj4_1U6'],
            'Payload': ['trojan4_0', 'trojan4_1']
            },
        'T205':{
            'Trigger': ['troj5_0U1', 'troj5_0U2', 'troj5_0U3', 'troj5_0U4', 'troj5_0U5', \
                    'troj5_0U6', 'troj5_0U7', \
                    'troj5_1U1', 'troj5_1U2', 'troj5_1U3', 'troj5_1U4', \
                    'troj5_1U5', 'troj5_1U6', 'troj5_1U7'],
            'Payload': ['trojan5_0', 'trojan5_1']
            },
        'T206':{
            'Trigger': ['troj6_0U1', 'troj6_0U2', 'troj6_0U3', 'troj6_0U4', 'troj6_0U5', \
                    'troj6_0U6', 'troj6_0U7'
                    'troj6_1U1', 'troj6_1U2', 'troj6_1U3', 'troj6_1U4', 'troj6_1U5', \
                    'troj6_1U6', 'troj6_1U7', 'troj6_1U8', 'troj6_1U9'],
            'Payload': ['trojan6_0', 'trojan6_1']
            },
        'T207':{
            'Trigger': ['troj7_0U1', 'troj7_0U2', 'troj7_0U3', 'troj7_0U4', 'troj7_0U5', 'troj7_0U6'\
                    'troj7_1U1', 'troj7_1U2', 'troj7_1U3', 'troj7_1U4', 'troj7_1U5', 'troj7_1U6', \
                    'troj7_1U7'],
            'Payload': ['trojan7_0', 'trojan7_1']
            },
        'T208':{
            'Trigger': ['troj8_0U1', 'troj8_0U2', 'troj8_0U3', 'troj8_0U4', \
                    'troj8_1U1', 'troj8_1U2', 'troj8_1U3', 'troj8_1U4', 'troj8_1U5', 'troj8_1U6'],
            'Payload': ['trojan8_0', 'trojan8_1']
            },
        'T209':{
            'Trigger': ['troj9_0U1', 'troj9_0U2', 'troj9_0U3', 'troj9_0U4', 'troj9_0U5'\
                    'troj9_1U1', 'troj9_1U2', 'troj9_1U3', 'troj9_1U4'],
            'Payload': ['trojan9_0', 'trojan9_1']
            },
        'T210':{
            'Trigger': ['troj10_0U1', 'troj10_0U2', \
                    'troj10_1U1', 'troj10_1U2', 'troj10_1U3', 'troj10_1U4', 'troj10_1U5'],
            'Payload': ['trojan10_0', 'trojan10_1']
            },
        'T211':{
            'Trigger': ['troj11_0U1', 'troj11_0U2', 'troj11_0U3', \
                    'troj11_0U4', 'troj11_0U5', 'troj11_0U6', \
                    'troj11_1U1', 'troj11_1U2', 'troj11_1U3', 'troj11_1U4', 'troj11_1U5'],
            'Payload': ['trojan11_0', 'trojan11_1']
            },
        'T212':{
            'Trigger': ['troj12_0U1', 'troj12_0U2', 'troj12_0U3', \
                    'troj12_1U1', 'troj12_1U2', 'troj12_1U3', 'troj12_1U4', \
                    'troj12_1U5', 'troj12_1U6', 'troj12_1U7'],
            'Payload': ['trojan12_0', 'trojan12_1']
            },
        'T213':{
            'Trigger': ['troj13_0U1', 'troj13_0U2', 'troj13_0U3', 'troj13_0U4', 'troj13_0U5', 'troj13_0U6', \
                    'troj13_1U1', 'troj13_1U2', 'troj13_1U3', 'troj13_1U4', \
                    'troj13_1U5', 'troj13_1U6', 'troj13_1U7'],
            'Payload': ['trojan13_0', 'trojan13_1']
            },
        'T214':{
            'Trigger': ['troj14_0U1', 'troj14_0U2', 'troj14_0U3', 'troj14_0U4', 'troj14_0U5', \
                    'troj14_0U6', \
                    'troj14_1U1', 'troj14_1U2', 'troj14_1U3', 'troj14_1U4', 'troj14_1U5', \
                    'troj14_1U6', 'troj14_1U7'],
            'Payload': ['trojan14_0', 'trojan14_1']
            },
        'T215':{
            'Trigger': ['troj15_0U1', 'troj15_0U2', 'troj15_0U3', 'troj15_0U4', 'troj15_0U5', \
                    'troj15_0U6', \
                    'troj15_1U1', 'troj15_1U2', 'troj15_1U3', 'troj15_1U4', 'troj15_1U5', \
                    'troj15_1U6', 'troj15_1U7', 'troj15_1U8'],
            'Payload': ['trojan15_0', 'trojan15_1']
            },
        'T216':{
            'Trigger': ['troj16_0U1', 'troj16_0U2', 'troj16_0U3', 'troj16_0U4', 'troj16_0U5', \
                    'troj16_0U6', \
                    'troj16_1U1', 'troj16_1U2', 'troj16_1U3', 'troj16_1U4', 'troj16_1U5'],
            'Payload': ['trojan16_0', 'trojan16_1']
            },
        'T217':{
            'Trigger': ['troj17_0U1', 'troj17_0U2', 'troj17_0U3', 'troj17_0U4', 'troj17_0U5', \
                    'troj17_0U6', \
                    'troj17_1U1', 'troj17_1U2', 'troj17_1U3', 'troj17_1U4', 'troj17_1U5', \
                    'troj17_1U6'],
            'Payload': ['trojan17_0', 'trojan17_1']
            },
        'T218':{
            'Trigger': ['troj18_0U1', 'troj18_0U2', 'troj18_0U3', 'troj18_0U4', 'troj18_0U5', \
                    'troj18_0U6', \
                    'troj18_1U1', 'troj18_1U2', 'troj18_1U3', 'troj18_1U4', \
                    'troj18_1U5', 'troj18_1U6'],
            'Payload': ['trojan18_0', 'trojan18_1']
            },
        'T219':{
            'Trigger': ['troj19_0U1', 'troj19_0U2', 'troj19_0U3', 'troj19_0U4', 'troj19_0U5', \
                    'troj19_0U6', \
                    'troj19_1U1', 'troj19_1U2', 'troj19_1U3', 'troj19_1U4', 'troj19_1U5', 'troj19_1U6', \
                    'troj19_1U7'],
            'Payload': ['trojan19_0', 'trojan19_1']
            },
        }
